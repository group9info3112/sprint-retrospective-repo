﻿/*
 *  File: ProjectData.cs
 *  Author: Bijan Khajavi, Leonid Osadchyi
 *  Date: March 20
 *  Purpose: Class definition for projectdata and deseralization copy
 * */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SprintRetrospective
{
    public class ProjectData : INotifyPropertyChanged
    {
        public ObservableCollection<Sprint> allSprints { get; set; }
        public ObservableCollection<UserStory> allStories { get; set; }
        public ObservableCollection<TeamMember> allTeamMembers { get; set; }

        public string teamName { get; set; }
        public string productName { get; set; }
        public string pivotalLink { get; set; }
        public DateTime startDate { get; set; }
        public int velocity { get; set; }
        public int hoursPerStoryPoint { get; set; }


        public ProjectData()
        {
            allSprints = new ObservableCollection<Sprint>();
            allStories = new ObservableCollection<UserStory>();
            allTeamMembers = new ObservableCollection<TeamMember>();
        }

        // build project data from a copycat class deserealizable by FIreSharp
        public ProjectData(ProjectDataDeserealizing pd)
        {
            this.allSprints = new ObservableCollection<Sprint>();
            allStories = new ObservableCollection<UserStory>();

            //if(pd.allStories)
            foreach (var story in pd.allStories)
            {
                if(story != null)
                    allStories.Add(new UserStory(story));
            }

            foreach (var sprint in pd.allSprints)
            {
                allSprints.Add(new Sprint(sprint));
            }

            this.allTeamMembers = new ObservableCollection<TeamMember>();

            foreach (var member in pd.allTeamMembers)
            {
                allTeamMembers.Add(member);
            }

            this.teamName = pd.teamName;
            this.productName = pd.productName;
            this.startDate = pd.startDate;
            this.velocity = pd.velocity;
            this.pivotalLink = pd.pivotalLink;
            this.hoursPerStoryPoint = pd.hoursPerStoryPoint;

        }

        public ProjectData(string projectName)
        {
            this.productName = projectName;
            allSprints = new ObservableCollection<Sprint>();
            allStories = new ObservableCollection<UserStory>();
            allTeamMembers = new ObservableCollection<TeamMember>();
        }

        public ProjectData(string tName, int tNum, string pName, string pLink, DateTime srtDate, int vel, int hpsp)
        {
            teamName = tName;
            productName = pName;
            pivotalLink = pLink;
            startDate = srtDate;
            velocity = vel;
            hoursPerStoryPoint = hpsp;

            allSprints = new ObservableCollection<Sprint>();
            allStories = new ObservableCollection<UserStory>();
            allTeamMembers = new ObservableCollection<TeamMember>();
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }

    // copycat class deserealizable by FIreSharp
    public class ProjectDataDeserealizing
    {
        public List<SprintDeserealising> allSprints { get; set; }
        public List<UserStoryDeserealizing> allStories { get; set; }
        public List<TeamMember> allTeamMembers { get; set; }

        public string teamName { get; private set; }
        public string productName { get; set; }
        public string pivotalLink { get; private set; }
        public DateTime startDate { get; private set; }
        public int velocity { get; private set; }
        public int hoursPerStoryPoint { get; private set; }


        public ProjectDataDeserealizing()
        {
            allSprints = new List<SprintDeserealising>();
            allStories = new List<UserStoryDeserealizing>();
            allTeamMembers = new List<TeamMember>();
        }

       
    }
}
