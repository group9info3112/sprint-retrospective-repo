/*
 *  Author: Leonid Osadchyi
 *  Date: 3 March 2019
 *  Purpose: UserStory class that will encapsulate data related to the user stories
 * 
 * */

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SprintRetrospective
{

    public enum Status
    {
        Planned,
        Started,
        Delivered
    }

    public class StoryTask : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public bool Done { get; set; }
        public string Task { get; set; }
    }

    public class UserStory : INotifyPropertyChanged
    {
        // fields
        public string ID { get; }
        public string Story { get; set; }

        public int InitialEstimate { get; set; }
        public double ActualHours { get; set; }

        private Status _status = Status.Planned;
        public Status CurrentStatus { get { return _status; }  set { _status = value; } }

        private int _sprint = -2;
        public int Sprint { get { return _sprint; } set { _sprint = value; } }

        //public List<string> TaskList { get; set; }
        public ObservableCollection<StoryTask> TaskList { get; set; }

        public TeamMember AssignedTo { get; set; }

        public string PercentageComplete
        {
            get
            {
                double numDone = 0;
                foreach (StoryTask task in TaskList)
                {
                    if (task.Done) { numDone++; }
                }
                if (TaskList.Count > 0)
                {
                    double result = (numDone / TaskList.Count) * 100.0;
                    return result + "%";
                }
                return "0%";
            }
            set { PercentageComplete = value; }
        }

        public int AssigneeID { get; }

        // constructors
        public UserStory() { TaskList = new ObservableCollection<StoryTask>(); }

        public UserStory(UserStoryDeserealizing us)
        {
            this.ActualHours = us.ActualHours;
            this.AssignedTo = us.AssignedTo;
            this.CurrentStatus = us.CurrentStatus;
            this.InitialEstimate = us.InitialEstimate;
            this.Sprint = us.Sprint;
            this.Story = us.Story;
            this.TaskList = new ObservableCollection<StoryTask>(us.TaskList);
        }

        public UserStory(int initialEstimate)
        {
            this.InitialEstimate = initialEstimate;
            TaskList = new ObservableCollection<StoryTask>();
            AssignedTo = new TeamMember();
        }
        public UserStory(string story)
        {
            this.Story = story;
            TaskList = new ObservableCollection<StoryTask>();
            AssignedTo = new TeamMember();
        }

        public UserStory(string story, int initialEstimate)
        {
            this.Story = story;
            this.InitialEstimate = initialEstimate;
            TaskList = new ObservableCollection<StoryTask>();
            AssignedTo = new TeamMember();
        }

        public UserStory(string story, int initialEstimate, int sprint)
        {
            this.Story = story;
            this.InitialEstimate = initialEstimate;
            this.Sprint = sprint;
            TaskList = new ObservableCollection<StoryTask>();
            AssignedTo = new TeamMember();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        // return a string containing all tasks for the user story
        public string BuildListOfTasks()
        {
            StringBuilder stringBuilder = new StringBuilder();

            int counter = 1;
            foreach (var task in TaskList)
            {
                stringBuilder.Append(counter + ". " + task);
            }

            return stringBuilder.ToString();
        }

        public int GetEstimatedHours(int hoursPerStoryPoint)
        {
            return this.InitialEstimate * hoursPerStoryPoint;
        }

        public double CalculateEstimatePercentage()
        {
            return (this.InitialEstimate / ActualHours - 1) * 100.0;
        }

        public double GetEstimatePercentage(string teamMember)
        {
            
            return 0;
        }


        // to string override
        public override string ToString()
        {
            return Story;
        }

        
    }

    // copycat class deserealizable by FIreSharp
    public class UserStoryDeserealizing
    {
        // fields
        public string ID { get; }
        public string Story { get; set; }

        public int InitialEstimate { get; set; }
        public double ActualHours { get; set; }

        private Status _status = Status.Planned;
        public Status CurrentStatus { get { return _status; } set { _status = value; } }

        private int _sprint = -2;
        public int Sprint { get { return _sprint; } set { _sprint = value; } }

        //public List<string> TaskList { get; set; }
        public List<StoryTask> TaskList { get; set; }

        public TeamMember AssignedTo { get; set; }

        public int AssigneeID { get; }

        // constructors
        public UserStoryDeserealizing() { TaskList = new List<StoryTask>();
            AssignedTo = new TeamMember();
        }

        

        


    }
}
