﻿/*
 *  File: ProjectData.cs
 *  Author: Bijan Khajavi
 *  Date: March 20
 *  Purpose: Class definition for TeamMember
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SprintRetrospective
{
    public class TeamMember
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public TeamMember() { }
        public TeamMember(string n)
        {
            this.Name = n;
        }

        public override string ToString()
        {
            return Name;
        }

    }
}
