/*
 *  Author: Leonid Osadchyi
 *  Date: 3 March 2019
 *  Purpose: Sprint class that will encapsulate data related to the sprint
 * 
 * */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SprintRetrospective
{
    public class Sprint
    {
        public int ID { get; }
        public int SprintNumber { get; set; }
        public List<UserStory> userStories { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }


        public Sprint()
        {

            startDate = System.DateTime.Now;
            endDate = System.DateTime.Now;
            userStories = new List<UserStory>();
        }

        public Sprint(SprintDeserealising sd)
        {
            userStories = new List<UserStory>();
            foreach (var story in sd.userStories)
            {
                userStories.Add(new UserStory(story));
            }

            this.SprintNumber = sd.SprintNumber;
            this.startDate = sd.startDate;
            this.endDate = sd.endDate;
        }

        // single arg constructor with the sprint number
        public Sprint(int number, DateTime start, DateTime end)
        {
            this.SprintNumber = number;
            this.startDate = start;
            this.endDate = end;
            userStories = new List<UserStory>();
        }


        // add user story to the sprint
        public void Add(UserStory story)
        {
            userStories.Add(story);
        }

        // remove user story from sprint
        public void Remove(string userStoryID)
        {
            for (int i = 0; i < userStories.Count; i++)
            {
                if (userStories[i].ID == userStoryID)
                {
                    userStories.Remove(userStories[i]);
                    break;
                }
            }
        }

        public string GetSummary()
        {
            StringBuilder sb = new StringBuilder();

            //sb.Append(this.ToString());
            sb.Append("Dates: " + this.startDate.ToString("dd-MM-yyyy") + " - " + this.endDate.ToString("dd-MM-yyyy") + "\n");
            sb.Append("Amount of user stories in sprint: " + this.userStories.Count + "\n");
            sb.Append("Amount of user stories planned: " + this.userStories.Where(story => story.CurrentStatus == Status.Planned).Count() + "\n");
            sb.Append("Amount of user stories in progress: " + this.userStories.Where(story => story.CurrentStatus == Status.Started).Count() + "\n");
            sb.Append("Amount of user stories delivered: " + this.userStories.Where(story => story.CurrentStatus == Status.Delivered).Count() + "\n");

            double totalHours = 0;
            this.userStories.ForEach(story => totalHours += story.ActualHours);

            sb.Append("Total hours spent: " + totalHours + "\n");



            return sb.ToString();
        }

        // tostring overide
        public override string ToString()
        {
            return "Sprint # " + this.SprintNumber;
        }
    }

    // copycat class deserealizable by FIreSharp
    public class SprintDeserealising
    {
        public int ID { get; }
        public int SprintNumber { get; set; }
        public List<UserStoryDeserealizing> userStories { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }


        public SprintDeserealising()
        {

            startDate = System.DateTime.Now;
            endDate = System.DateTime.Now;
            userStories = new List<UserStoryDeserealizing>();
        }
    }
}
