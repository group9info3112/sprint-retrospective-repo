﻿/*
 *  File: DatabaseManager.cs
 *  Author: Leonid Osadchyi
 *  Date: February 28
 *  Purpose: DatabaseManager class definition for accessing and manipulating firesharp db
 * */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FireSharp.Config;
using FireSharp.Interfaces;
using FireSharp.Response;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SprintRetrospective
{
    public class DatabaseManager
    {
        private IFirebaseConfig config = new FirebaseConfig
        {
            AuthSecret = "DdUwbu8C0XYyXIJSJJ3OKA0ihkV6nfBTiZ26dnRZ",
            BasePath = "https://sprint-retrospective-2ef1e.firebaseio.com/"
        };
       // private Guid guid;
        private IFirebaseClient client;

        // ctor that establishes the connection
        public DatabaseManager()
        {
            client = new FireSharp.FirebaseClient(config);
        }

        // save project function
        public void AddProject(ProjectData project)
        {
            try
            {

                SetResponse response = client.Set("Projects/" + project.productName, project);

            }
            catch(Exception ex)
            {
                throw new Exception("Database Error on Add!: " + ex.Message);
            }
        }

        // get project from the database function
        public async Task<ProjectData> GetProjectByName(string projName)
        {
            try { 
                FirebaseResponse response = client.Get("Projects/" + projName);
                var deserealized = response.ResultAs<ProjectDataDeserealizing>();

                var resultObj = new ProjectData(deserealized);

                return resultObj;
            }
            catch(Exception ex)
            {
                throw new Exception("Database Error on Get!: " + ex.Message);
            }
        }


        // delete project from the database
        public async Task<bool> DeleteProject(string projName)
        {
            try
            {
                DeleteResponse response = await client.DeleteTaskAsync("Projects/" + projName);
                var resultObj = response.Success;
                return resultObj;
            }
            catch (Exception ex)
            {
                throw new Exception("Database Error on Delete!: " + ex.Message);
            }

        }

        // get all project names stored in the db
        public List<string> GetAllProjectNames()
        {
            try
            {
                FirebaseResponse response = client.Get("Projects");

                var rer = response.ResultAs<List<string>>();
                JObject resultObj = JsonConvert.DeserializeObject<JObject>( rer.First() );

                List<string> names = new List<string>();

                foreach (var item in resultObj)
                {
                    names.Add(item.Key);
                }

                return names;
            }
            catch (Exception ex)
            {
                throw new Exception("Database Error!: " + ex.Message);
            }
        }

     

    }
}
