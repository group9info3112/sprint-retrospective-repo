﻿using SprintRetrospective.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
namespace SprintRetrospective.View
{
    /// <summary>
    /// Interaction logic for DeveloperManager.xaml
    /// Author: Bijan Khajavi
    /// Date: April 3
    /// </summary>
    public partial class DeveloperManager : Window
    {
        ViewModelDashboard vm;

        bool editing = false;
        string originalName;
        int originalIndex = 0;

        public List<TeamMember> developers;

        public TeamMember SelectedTeamMember
        {
            get { return (TeamMember)GetValue(SelectedDeveloperProperty); }
            set { SetValue(SelectedDeveloperProperty, value); }
        }

        public static readonly DependencyProperty SelectedDeveloperProperty =
            DependencyProperty.Register("SelectedTeamMember", typeof(TeamMember), typeof(DeveloperManager), new PropertyMetadata(null));

        // ctor
        public DeveloperManager(ObservableCollection<TeamMember> members, ViewModelDashboard viewModel)
        {
            InitializeComponent();

            this.vm = viewModel;

            this.DataContext = this;

            // set title of the window to the group name
            if(viewModel.SelectedProject.teamName != null)
            {
                this.Title = viewModel.SelectedProject.teamName;
            }

            this.developers = new List<TeamMember>();

            foreach(var member in members)
            {
                this.developers.Add(member);
            }
            foreach (var member in developers)
            {
                lbDevelopers.Items.Add(member.Name);
            }
            if (developers.Count > 0)
            {
                lbDevelopers.SelectedIndex = originalIndex;
                tb_MemberName.IsEnabled = true;
            }
        }

        // event handler to fire when window is shutting down
        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(validateName())
            {
                // pass the data to the view model
                this.vm.UpdateDeveloperData(developers);
            }
            else
            {
                e.Cancel = true;
            }
        }

        // selection changed on developers list box handler
        private void LbDevelopers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(lbDevelopers.Items.Count > 0)
            {
                if (validateName())
                {
                    SelectedTeamMember = developers[lbDevelopers.SelectedIndex];
                    originalName = SelectedTeamMember.Name;
                    originalIndex = lbDevelopers.SelectedIndex;
                    tb_MemberName.Text = originalName;
                    var userStories = vm.SelectedProject.allStories.Where(u => u.AssignedTo.Name == originalName).ToList();
                    if(userStories.Count > 0)
                    {
                        double accuracy = vm.CalculateTotalEstimatePercentage(originalName);
                        tb_EstimateAccuracy.Text = accuracy.ToString() + "%";
                    }
                }
            }
        }

        // new member button clicked
        private void NewMemberBtn_Click(object sender, RoutedEventArgs e)
        {
            if (validateName())
            {
                TeamMember newMember = new TeamMember();
                int memberCount = 1;
                string testName = "";
                while (true)
                {
                    testName = "Member " + (developers.Count + memberCount);
                    if (checkExistence(testName)) { memberCount++; }
                    else { break; }
                }
                newMember.Name = testName;
                
                SelectedTeamMember = newMember;
                originalName = testName;
                originalIndex = lbDevelopers.SelectedIndex;
                tb_MemberName.Text = originalName;
                tb_EstimateAccuracy.Text = "";

                developers.Add(SelectedTeamMember);
                lbDevelopers.Items.Add(newMember.Name);
                lbDevelopers.SelectedIndex = lbDevelopers.Items.Count - 1;

                tb_MemberName.IsEnabled = true;
            }
        }

        private void DeleteMemberBtn_Click(object sender, RoutedEventArgs e)
        {
            //Add functionality to remove Team member from user stories
            //for now just remove from list
            developers.Remove(SelectedTeamMember);
            UpdateDeveloperList();
            if (developers.Count > 0) { lbDevelopers.SelectedIndex = 0; }
            else {
                tb_MemberName.Text = "";
                tb_MemberName.IsEnabled = false;
            }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            editing = true;
        }

        // check if developer name already exists
        private bool checkExistence(string testName)
        {
            return developers.Where(member => member.Name == testName).ToList().Count > 0;
        }

        // validate the names
        private bool validateName()
        {
            if(tb_MemberName.IsEnabled && tb_MemberName.Text != originalName && tb_MemberName.Text != "" && editing)
            {
                if (checkExistence(tb_MemberName.Text))
                {
                    MessageBox.Show("Error: Name entered already exists in the list of developers!\nYou cannot select another team member or exit this window until you either change the name or remove the team member.");
                    return false;
                }
                else
                {
                    //save data
                    editing = false;
                    UpdateDeveloperList();
                }
            }  
            return true;
        }

        private void UpdateDeveloperList()
        {
            if(developers.Count > 0)
            {
                SelectedTeamMember.Name = tb_MemberName.Text;
            }
            lbDevelopers.Items.Clear();
            foreach (var member in developers)
            {
                lbDevelopers.Items.Add(member.Name);
            }
            if(developers.Count > 0)
            {
                lbDevelopers.SelectedIndex = originalIndex;
            }
        }

        private void Btn_UpdateListBox_Click(object sender, RoutedEventArgs e)
        {
            validateName();
        }
    }
}
