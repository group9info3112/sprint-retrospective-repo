﻿using SprintRetrospective.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SprintRetrospective.View
{
    /// <summary>
    /// Interaction logic for SprintsDialog.xaml allowing editing the list of sprints for the project
    /// Author: Leonid Osadchyi
    /// Date: April 4
    /// </summary>
    public partial class SprintsDialog : Window
    {

        ViewModelDashboard vm;

        public List<Sprint> sprints;

        public List<UserStory> deletedStories;

        public Sprint SelectedSprint
        {
            get { return (Sprint)GetValue(SelectedSprintProperty); }
            set { SetValue(SelectedSprintProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedSprint.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedSprintProperty =
            DependencyProperty.Register("SelectedSprint", typeof(Sprint), typeof(SprintsDialog), new PropertyMetadata(null));



        // ctor
        public SprintsDialog(ObservableCollection<Sprint> sprints, ViewModelDashboard viewModel)
        {
            InitializeComponent();

            this.vm = viewModel;

            this.DataContext = this;

            this.sprints = new List<Sprint>();
            // keep track of deleted stories
            this.deletedStories = new List<UserStory>();

            foreach (var sprint in sprints)
            {
                DatesPanel.IsEnabled = true;
                this.sprints.Add(sprint);
                lbSprints.Items.Add(sprint.SprintNumber);
            }
        }

        private void LbSprints_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            SelectedSprint = sprints[lbSprints.SelectedIndex];
        }

        // new sprint button
        private void NewSprintBtn_Click(object sender, RoutedEventArgs e)
        {
            Sprint newSprint = new Sprint();

            try
            {
                // sprints are week long be default
                if(sprints.Count > 0)
                {
                    newSprint.startDate = sprints[sprints.Count - 1].endDate.AddDays(1);
                    newSprint.endDate = newSprint.startDate.AddDays(7);
                }
                //int newSprintNum = 1;
                //if (lbSprints.Items.Count != 0)
                //    newSprintNum = Int32.Parse(lbSprints.Items[lbSprints.Items.Count - 1].ToString()) + 1;
                int newSprintNum = lbSprints.Items.Count + 1;


                lbSprints.Items.Add(newSprintNum);

                newSprint.SprintNumber = newSprintNum;
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error! " + ex.Message, "Error",MessageBoxButton.OK,MessageBoxImage.Error);
            }

            SelectedSprint = newSprint;

            sprints.Add(SelectedSprint);

            DatesPanel.IsEnabled = true;
        }

        // update sprints
        private void UpdateSprints(object sender, RoutedEventArgs e)
        {
            foreach(var s in sprints)
            {
                if(s.SprintNumber == SelectedSprint.SprintNumber)
                {
                    s.startDate = SelectedSprint.startDate;
                    s.endDate = SelectedSprint.endDate;
                }
            }
        }

        // delete sprint button
        private void DeleteSprintBtn_Click(object sender, RoutedEventArgs e)
        {
            // validate
            if (lbSprints.SelectedIndex == 0)
            {
                MessageBox.Show("Cannot Remove First Sprint!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else if (lbSprints.SelectedIndex < 0)
            {
                MessageBox.Show("Select Latest Sprint to Delete Sprints.", "Error", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            else
            {
                if (lbSprints.SelectedIndex == lbSprints.Items.Count - 1)
                {
                    // store user stories first
                    foreach (var story in sprints[sprints.Count - 1].userStories)
                    {
                        deletedStories.Add(story);
                    }

                    SelectedSprint = sprints[sprints.Count - 2];
                    var selected = lbSprints.SelectedIndex;
                    lbSprints.SelectedIndex = 0;
                    lbSprints.Items.RemoveAt(selected);
                    sprints.RemoveAt(sprints.Count - 1);

                }
                else
                {
                    MessageBox.Show("Error: Can only Remove Most Recent Sprint!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        // send data to vm on closing
        private void OnClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            this.vm.UpdateSprintData(this.sprints, this.deletedStories);

           // this.Close();
        }
    }
}
