﻿using SprintRetrospective.View;
using SprintRetrospective.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SprintRetrospective
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// Author: Leonid Osadchyi
    /// Date: March 23
    /// </summary>
    public partial class Dashboard : Window
    {
        // vm that will be the datacontext for the window
        ViewModelDashboard vm;

        // ctor that accepts a viewmodel
        public Dashboard(ViewModelDashboard vm)
        {
            InitializeComponent();
            this.vm = vm;

            // populate the backlog listbox with stories
            foreach (var story in vm.SelectedProject.allStories)
            {
                if (story.Sprint == -2)
                    lbStories.Items.Add(story.Story);
            }

            this.DataContext = this.vm;
        }

        // new story button clicked
        private void btn_newStory_Click(object sender, RoutedEventArgs e)
        {
            // make view model add a new user story
            vm.AddUserStory();

            // sort out the listbox
            lbStories.Items.Clear();
            foreach(var story in vm.SelectedProject.allStories)
            {
                if (story.Sprint == -2)
                    lbStories.Items.Add(story.Story);
            }
            lbStories.SelectedIndex = lbStories.Items.Count - 1;
        }

        // on window closing
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // save the project data
            vm.SaveProject();
            ((App)Application.Current).Shutdown();
        }

        // delete user story button click
        private void btn_delete_Click(object sender, RoutedEventArgs e)
        {
            // make sure that the backlog is in focus

                vm.DeleteStory(lbStories.SelectedIndex);

                lbStories.Items.Clear();
                foreach (var story in vm.SelectedProject.allStories)
                {
                    lbStories.Items.Add(story.Story);
                }
        }

        // update story button click
        private void btn_update_Click(object sender, RoutedEventArgs e)
        {
            lbStories.Items.Clear();
            foreach (var story in vm.SelectedProject.allStories)
            {
                if(story.Sprint == -2)
                 lbStories.Items.Add(story.Story);
            }

            // set percentage completed
            vm.setPercentage(tb_complete);
        }

        // on selected
        private void lbStories_Selected(object sender, RoutedEventArgs e)
        {
            if(lbStories.SelectedIndex >= 0)
            {
                vm.SelectUserStory((string)lbStories.SelectedValue);
                //MessageBox.Show(dg_taskList.Items.Count+"");
            }
        }

        // sprints menu button clicked
        private void btn_sprints_Click(object sender, RoutedEventArgs e)
        {
            vm.ShowSprintsWindow();
        }

        // developers menu button clicked
        private void btn_developers_Click(object sender, RoutedEventArgs e)
        {
            vm.ShowDevelopersWindow();
        }

        private void lbSprintStories_Selected(object sender, SelectionChangedEventArgs e)
        {
            if (lbSprintStories.SelectedIndex >= 0 && cb_sprintNumber.SelectedIndex >= 0)
            {
                //vm.SelectUserStory(lbSprintStories.SelectedIndex, cb_sprintNumber.SelectedIndex);
                vm.SelectUserStory((UserStory)lbSprintStories.SelectedValue);
                //MessageBox.Show(dg_taskList.Items.Count+"");
            }
            storyPanel.IsEnabled = true;
        }

        private void Cb_sprint_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            vm.AddStoryToSprint(((ComboBox)sender).SelectedIndex);
        }

        // project info button clicked
        private void btn_rename_Click(object sender, RoutedEventArgs e)
        {
            vm.ShowRenameProj();
        }

        // summary button clicked
        private void btn_summary_Click(object sender, RoutedEventArgs e)
        {
            vm.ShowSummary();
        }

        private void lbSprintStories_Selected(object sender, RoutedEventArgs e)
        {
            if (lbSprintStories.SelectedIndex >= 0 && cb_sprintNumber.SelectedIndex >= 0)
            {
                //vm.SelectUserStory(lbSprintStories.SelectedIndex, cb_sprintNumber.SelectedIndex);
                vm.SelectUserStory((UserStory)lbSprintStories.SelectedValue);
                //MessageBox.Show(dg_taskList.Items.Count+"");
            }
            storyPanel.IsEnabled = true;
        }
    }
}
