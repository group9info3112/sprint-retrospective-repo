﻿using SprintRetrospective.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;



namespace SprintRetrospective
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml Starting window for the app
    /// Author: Leonid Osadchyi
    /// Date: March 23
    /// </summary>
    public partial class MainWindow : Window
    {
        ViewModelProjectWindow vm;

        public MainWindow()
        {
            InitializeComponent();
            vm = new ViewModelProjectWindow();
            this.DataContext = vm;

        }

        // new project button
        private void newProject_onClick(object sender, RoutedEventArgs e)
        {
            vm.NewProject();
            this.Close();
        }

        // delete project button
        private void delete_onClick(object sender, RoutedEventArgs e)
        {
            vm.DeleteProject(cb_Projects.SelectedValue.ToString());
        }

        // open project button
        private void openProject_onClick(object sender, RoutedEventArgs e)
        {
            if (cb_Projects.SelectedIndex > -1)
            {
                vm.OpenProject(cb_Projects.SelectedValue.ToString());
                this.Close();
            }
        }

    }
}
