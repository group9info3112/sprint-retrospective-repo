﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SprintRetrospective.View
{
    /// <summary>
    /// Interaction logic for SprintSummary.xaml
    /// Author: Leonid Osadchyi
    /// Date: April 7
    /// </summary>
    public partial class SprintSummary : Window
    {
         // simple window to store the return of getsummary function
        public SprintSummary(Sprint sprint)
        {
            InitializeComponent();

            tbSprintSummary.Text = sprint.GetSummary();
            gbSprintSummary.Header = sprint.ToString();
        }
    }
}
