﻿using SprintRetrospective.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SprintRetrospective.View
{
    /// <summary>
    /// Interaction logic for Projectname.xaml
    /// Author: Leonid Osadchyi
    /// Date: April 6
    /// </summary>
    public partial class ProjectName : Window
    {
        private DatabaseManager db;
        private ViewModelDashboard vm;
        private ViewModelProjectWindow vmp;
        private string oldname;




        public ProjectData SelectedProject
        {
            get { return (ProjectData)GetValue(SelectedProjectProperty); }
            set { SetValue(SelectedProjectProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedProject.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedProjectProperty =
            DependencyProperty.Register("SelectedProject", typeof(ProjectData), typeof(ProjectName), new PropertyMetadata(null));


        // ctor that accepts an existing project data disables editable fields
        public ProjectName(ViewModelDashboard vm, ProjectData proj)
        {
            InitializeComponent();
            db = new DatabaseManager();
            this.SelectedProject = proj;
            this.vm = vm;
            //ControlPanel. = true;
            btnRename.Visibility = Visibility.Hidden;

            this.DataContext = this;

            tbNewname.IsReadOnly = true;
            tbHrs.IsReadOnly = true;
            tbLink.IsReadOnly = true;
            tbTeamName.IsReadOnly = true;
            tbVelocity.IsReadOnly = true;
            dpStart.IsEnabled = false;


        }

        // ctor for when a brand new project is being made (fields editable)
        public ProjectName(ViewModelProjectWindow vm)
        {
            InitializeComponent();
            db = new DatabaseManager();
            this.oldname = "";
            this.vmp = vm;
            this.DataContext = this;
            SelectedProject = new ProjectData("", 0, "", "", DateTime.Now, 6, 8);
        }

        public ProjectName(ViewModelDashboard vm, string oldname, DatabaseManager db)
        {
            InitializeComponent();
            this.oldname = oldname;
            this.db = db;
            this.vm = vm;
        }

        // set info button
        private void btnRename_Click(object sender, RoutedEventArgs e)
        {
            // if viewmodel exists
            if (vm != null)
            {
                // validate fields
                if (tbNewname.Text.Length == 0)
                    lblStatus.Content = "Project name can't be empty";
                else if (SelectedProject.teamName.Length == 0)
                    lblStatus.Content = "Team name can't be empty";
                else
                {
                    var projectNames = db.GetAllProjectNames();

                    if (projectNames.Contains(tbNewname.Text))
                    {
                        lblStatus.Content = "Project name entered is already in use";
                    }
                    else
                    {
                        vm.RenameProject(tbNewname.Text);
                        this.Close();
                    }
                }
            }
            else
            {
                // validate fields
                if (tbNewname.Text.Length == 0)
                    lblStatus.Content = "Project name can't be empty";
                else if (SelectedProject.teamName.Length == 0)
                    lblStatus.Content = "Team name can't be empty";
                else
                {
                    var projectNames = db.GetAllProjectNames();

                    if (projectNames.Contains(tbNewname.Text))
                    {
                        lblStatus.Content = "Project name entered is already in use";
                    }
                    else
                    {
                        vmp.RenameProject(this.SelectedProject);
                        this.Close();
                    }
                }
            }
        }

        private void tbNewname_GotFocus(object sender, RoutedEventArgs e)
        {
            lblStatus.Content = "";
        }
    }
}
