﻿/*
 *  File: ViewModelDashboard.cs
 *  Author: Leonid Osadchyi, Bijan Khajavi
 *  Date: March 12
 *  Purpose: View model for the Dashboard window
 * */

using MaterialDesignThemes.Wpf;
using SprintRetrospective.View;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SprintRetrospective.ViewModel
{
    public class ViewModelDashboard : DependencyObject
    {
        // selected project
        public ProjectData SelectedProject
        {
            get { return (ProjectData)GetValue(SelectedProjectProperty); }
            set { SetValue(SelectedProjectProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedProject.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedProjectProperty =
            DependencyProperty.Register("SelectedProject", typeof(ProjectData), typeof(ViewModelProjectWindow), new PropertyMetadata(null));

        // status enumeration
        public IEnumerable<Status> StatusEnum
        {
            get
            {
                return Enum.GetValues(typeof(Status))
                    .Cast<Status>();
            }
        }

        // selected story
        public UserStory SelectedStory
        {
            get { return (UserStory)GetValue(SelectedStoryProperty); }
            set { SetValue(SelectedStoryProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedStory.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedStoryProperty =
            DependencyProperty.Register("SelectedStory", typeof(UserStory), typeof(ViewModelDashboard), new PropertyMetadata(null));

        // currently selected sprint
        public Sprint SelectedSprint
        {
            get { return (Sprint)GetValue(SelectedSprintProperty); }
            set { SetValue(SelectedSprintProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedSprint.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedSprintProperty =
            DependencyProperty.Register("SelectedSprint", typeof(Sprint), typeof(ViewModelDashboard), new PropertyMetadata(null));

        // set percentage function that updates passed in textbox with a new completion percentage
        public void setPercentage(TextBox complete)
        {
            complete.Text = SelectedStory.PercentageComplete;
        }

        // ctor accepting the project data from the project selection window
        public ViewModelDashboard(ProjectData project)
        {
            SelectedProject = project;

            Dashboard dashboard = new Dashboard(this);
        }

        // update sprint data that updates currently selected project's sprint and story lists
        public void UpdateSprintData(List<Sprint> sprints, List<UserStory> deletedStories)
        {
            SelectedProject.allSprints.Clear();

            foreach (var item in sprints)
            {
                SelectedProject.allSprints.Add(item);
            }

            foreach (var item in deletedStories)
            {
                SelectedProject.allStories.Add(item);
            }
        }

        // save project function that accesses the db and saves the project by name
        public void SaveProject()
        {
            DatabaseManager db = new DatabaseManager();

            db.AddProject(SelectedProject);
        }

        // update developer data that updates project's developer lists
        public void UpdateDeveloperData(List<TeamMember> members)
        {
            //string assigned = SelectedStory.AssignedTo.Name;
            SelectedProject.allTeamMembers.Clear();
            foreach(var item in members)
            {
                SelectedProject.allTeamMembers.Add(item);
            }
            //Do User Story stuff here!
        }

        // add new user story to the project
        public void AddUserStory()
        {
            int userStoryCount = SelectedProject.allStories.Count + 1;
            SelectedProject.allStories.Add(new UserStory("User Story "+userStoryCount));
        }

        // delete story from the backlog and project data
        public void DeleteStory(int selectedIndex)
        {
            SelectedProject.allStories.RemoveAt(selectedIndex);
        }

        // update user story selection
        public void SelectUserStory(string selectedStory)
        {
            foreach(var story in SelectedProject.allStories)
            {
                if (story.Story == selectedStory)
                    SelectedStory = story;

            }
        }

        // create and show sprint summary window
        public void ShowSummary()
        {
            SprintSummary summary = new SprintSummary(SelectedSprint);

            summary.Show();
        }

        // create and show project info window
        public void ShowRenameProj()
        {
            ProjectName pn = new ProjectName(this, SelectedProject);

            pn.ShowDialog();
        }

        // rename project (currently unused)
        public async void RenameProject(string newName)
        {
            DatabaseManager db = new DatabaseManager();
            await db.DeleteProject(SelectedProject.productName);

            SelectedProject.productName = newName;

           // await db.AddProject(SelectedProject);
        }

        // estimate percentage calculation
        public double CalculateTotalEstimatePercentage(string teamMember)
        {
            List<UserStory> assignments = new List<UserStory>();
            foreach (var story in SelectedProject.allStories)
            {
                if (story.Sprint == -2)
                {
                    if (story.AssignedTo.Name == teamMember)
                        assignments.Add(story);
                }
            }

            foreach(var sprint in SelectedProject.allSprints)
            {
                foreach(var story in sprint.userStories)
                {
                    if (story.AssignedTo.Name == teamMember)
                        assignments.Add(story);
                }
            }

            // = allStories.Select(member => member.AssignedTo == teamMember);
            if (assignments.Count > 0)
            {
                double initial = 0;
                double actual = 0;
                foreach (UserStory us in assignments)
                {
                    if (us.CurrentStatus == Status.Delivered)
                    {
                        initial += us.InitialEstimate;
                        actual += us.ActualHours;
                    }
                }
                if (initial <= 0 || actual <= 0)
                {
                    return 0.0;
                }
                else if (initial == actual)
                    return 100.0;
                else
                    return (initial / actual - 1) * 100.0;
            }
            else
            {
                return 0.0;
            }

        }

        // add story to a sprint function
        public void AddStoryToSprint(int sprintIndex)
        {
            // remove from backlog list or other sprints
            foreach(var sprint in SelectedProject.allSprints)
            {
                for (int i = 0; i < sprint.userStories.Count; i++)
                {
                    if(sprint.userStories[i].Story == SelectedStory.Story)
                    {
                        
                        sprint.userStories.RemoveAt(i);
                        break;
                    }
                }
            }

            // add story to sprint list
            if (sprintIndex >= 0)
                SelectedProject.allSprints[sprintIndex].Add(SelectedStory);
            else
            {
                foreach (var story in SelectedProject.allStories)
                {
                    if (story.Story == SelectedStory.Story)
                        story.Sprint = -2;
                }
                //SelectedStory.Sprint = -2;
            }

            
        }

        public void SelectUserStory(UserStory story)
        {
            SelectedStory = story;
            //  SelectedStory = SelectedProject.allSprints[sprintIndex].userStories[storyIndex];
        }

        // show sprint dialog window for managing sprints
        public void ShowSprintsWindow()
        {
            SprintsDialog sprintWindow = new SprintsDialog(this.SelectedProject.allSprints, this);
            sprintWindow.ShowDialog();
        }

        // show developer dialog window for managing developers
        public void ShowDevelopersWindow()
        {
            DeveloperManager developerWindow = new DeveloperManager(this.SelectedProject.allTeamMembers,this);
            developerWindow.ShowDialog();
        }

       
    }
}
