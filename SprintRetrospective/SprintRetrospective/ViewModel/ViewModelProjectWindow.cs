﻿/*
 *  File: ViewModelProjectWindow.cs
 *  Author: Leonid Osadchyi, Bijan Khajavi
 *  Date: March 12
 *  Purpose: View model for the Project selection window
 * */

using MaterialDesignThemes.Wpf;
using SprintRetrospective.View;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SprintRetrospective.ViewModel
{
    public class ViewModelProjectWindow : DependencyObject
    {



        public ObservableCollection<string> ProjectList
        {
            get { return (ObservableCollection<string>)GetValue(ProjectListProperty); }
            set { SetValue(ProjectListProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ProjectList.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ProjectListProperty =
            DependencyProperty.Register("ProjectList", typeof(ObservableCollection<string>), typeof(ViewModelProjectWindow), new PropertyMetadata(null));

        public Dashboard CurrentDashboard { get; set; }

        // ctor
        public ViewModelProjectWindow()
        {
            try
            {
                ProjectList = new ObservableCollection<string>();

                DatabaseManager dm = new DatabaseManager();

                foreach (var proj in dm.GetAllProjectNames())
                {
                    ProjectList.Add(proj);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        // call dbmanager to delete a project
        public async void DeleteProject(string name)
        {
            try
            {
                DatabaseManager dm = new DatabaseManager();

                await dm.DeleteProject(name);

                ProjectList.Clear();

                foreach (var proj in dm.GetAllProjectNames())
                {
                    ProjectList.Add(proj);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
}

        // call db manager to get project data and open dashboard
        public async void OpenProject(string name)
        {
            try
            {
                DatabaseManager dm = new DatabaseManager();

                var project = await dm.GetProjectByName(name);

                CurrentDashboard = new Dashboard(new ViewModelDashboard(project));

                CurrentDashboard.Show();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        // opens dashboard after setting project info
        public void RenameProject(ProjectData proj)
        {
            CurrentDashboard = new Dashboard(new ViewModelDashboard(proj));

            CurrentDashboard.Show();
        }

        // new project clicked - open project info window
        public void NewProject()
        {
            var NamingWindow = new ProjectName(this);

            NamingWindow.ShowDialog();
        }

    }
}
